package config

import(
	"fmt"
	"github.com/caarlos0/env"
	"github.com/joho/godotenv")

// Configuration contains static inform required to run apps and DB infor
type Configuration struct {
	Address string `env:"ADDRESS" envDefault:":8080"`
	JwtSecret string `env:"JWT_SECRET,required"`
	DataBaseConnectionURL string `env:"CONNECTION_URL,required"`
	DataBaseName          string `env:"DATABASE_NAME,required"`
}

func NewConfig() *Configuration {
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Println("No .env file could be found.")
	}
	cfg := Configuration{}
    err = env.Parse(&cfg)
    if err != nil {
    	fmt.Printf("%s", err)
	}
	return &cfg
}