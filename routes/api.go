package routes

import (
	"github.com/gorilla/mux"
	mgo "gopkg.in/mgo.v2"
	"mongodb-restful/config"
	api "mongodb-restful/handlers"
	userRepo "mongodb-restful/repositories/user"
	authSrv "mongodb-restful/services/auth"
	_ "mongodb-restful/services/user"
	"net/http"
)

var baseRoute = "/api/v1"

func InitializeRoutes(router *mux.Router, dbSession *mgo.Session, conf *config.Configuration) {
	userRepository := userRepo.New(dbSession, conf)
	//userService := userSrv.New(userRepository)
	authService := authSrv.New(userRepository)
	authAPI := api.NewAuthAPI(authService, conf)
	//userAPI := api.NewUserAPI(userService)

	// Router
	router.HandleFunc(baseRoute+ "auth/register", authAPI.Create).Methods(http.MethodPost)
	router.HandleFunc(baseRoute+"/auth/login", authAPI.Login).Methods(http.MethodPost)

}