package handlers

import userSrv "mongodb-restful/services/user"

// UserHandler - handles user request
type UserHandler struct {
	us userSrv.UserServiceInterface
}

func NewUserAPI(userService userSrv.UserServiceInterface) *UserHandler {
	return &UserHandler{
		us: userService,
	}
}

