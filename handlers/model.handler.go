package handlers

import "mongodb-restful/models"

type signupReq struct {
	Name string `json:name`
	Email string `json:email`
	Password string `json:password`
}

type loginRes struct {
	Token string `json:"token"`
	User *models.User `json:"user"`
}