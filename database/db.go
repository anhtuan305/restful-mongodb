package db

import (
	mgo "gopkg.in/mgo.v2"
	"mongodb-restful/config"
)

var instanace *mgo.Session
var err error

func GetInstance (c *config.Configuration) *mgo.Session {
	if instanace == nil {
		instanace, err = mgo.Dial(c.DataBaseConnectionURL)
		if err != nil {
			panic(err)
		}
	}
	return instanace.Copy()
}