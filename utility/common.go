package utility

import "time"

//get milliseconds of given time
func UnixMilli(t time.Time) int64 {
	return t.Round(time.Millisecond).UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
}

// CurrentTimeInMilli use to get current time in milliseconds
func CurrentTimeInMilli() int64 {
	return UnixMilli(time.Now())
}