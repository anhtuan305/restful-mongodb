package utility

import (
	"errors"
	"regexp"
	"strconv"
)

//Error codes
const (
	InvalidUserID       = "invalidUserID" // in case userid not exists
	InternalError       = "internalError" // in case, any internal server error occurs
	UserNotFound        = "userNotFound"  // if user not found
	InvalidBindingModel = "invalidBindingModel"
	EntityCreationError = "entityCreationError"
	Unauthorized        = "unauthorized" // in case, try to access restricted resource
	BadRequest          = "badRequest"
	UserAlreadyExists   = "userAlreadyExists"
)

// Error code with description
var errorMessage = map[string]string {
	"invalidUserID":       "invalid user id",
	"internalError":       "an internal error occured",
	"userNotFound":        "user could not be found",
	"invalidBindingModel": "model could not be bound",
	"entityCreationError": "could not create entity",
	"unauthorized":        "an unauthorized access",
	"userAlreadyExists":   "user already exists",
}

// ValidateRequireAndLengthAndRegex is used to validate any input data
// @params  value is input data
// @params isRequired defines the input value required or not
// @params minLength defines minimum length of the input value
// @params maxLength defines maximum length of the input value
// @params regex defines the feature of the input value, "" value define no regex required
func ValidateRequireAndLengthAndRegex(value string, isRequired bool, minLength, maxLength int, regex, fieldName string) error {
	length := len(value)
	Re := regexp.MustCompile(regex)
	if isRequired == true && length < 1 {
		return errors.New(fieldName + "is Required")
	}

	if minLength != 0 && length > 1 && length < minLength {
		return errors.New(fieldName + "must be min"+  strconv.Itoa(minLength))
	}

	if maxLength != 0 && length > 0 && length > maxLength {
		return errors.New(fieldName + "must be max"+ strconv.Itoa(maxLength))

	}

	if !Re.MatchString(value) {
		return errors.New("Invalid" + fieldName)
	}
	return nil
}

func NewHTTPError(errorCode string, statusCode int) map[string]interface{} {
	m := make(map[string]interface{})
	m["error"] = errorCode
	m["error_description"] = errorMessage[errorCode]
	m["code"] = statusCode

	return m
}

// Send http response if  any error occurs
func NewHTTPCustomError(errorCode, errorMsg string, statusCode int) map[string]interface{} {
	m := make(map[string]interface{})
	m["error"] = errorCode
	m["error_description"] = errorMsg
	m["code"] = statusCode

	return m
}