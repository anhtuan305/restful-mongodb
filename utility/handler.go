package utility

import (
	"encoding/json"
	"github.com/gorilla/handlers"
	"net/http"
)

// Response return json response of http
func Response(w http.ResponseWriter, payload interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode(payload)
}

// Headers set header to request
func Headers(r http.Handler) http.Handler {
	headersok := handlers.AllowedHeaders([]string{"Authorization"})
	originsok := handlers.AllowedOrigins([]string{"*"})
	methodsok := handlers.AllowedMethods([]string {"GET", "POST", "PUT", "OPTION"})
	return handlers.CORS(headersok, originsok, methodsok)(r)
}

func SuccessPayload(data interface{}, message string, args ...int) map[string]interface{} {
	result := make(map[string]interface{})
	result["data"] = data
	result["message"] = message
	if len(args) == 0 {
		result["code"] = 200
	} else {
		result["code"] = args[0]
	}
	return result
}



