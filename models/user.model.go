package models

import (
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
	"mongodb-restful/utility"
)

type User struct {
	ID        bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	Name      string        `json:"name,omitempty" bson:"name,omitempty"`
	Email     string        `json:"email,omitempty" bson:"email,omitempty"`
	Password  string        `json:"-" bson:"password,omitempty"`
	Salt      string        `json:"-" bson:"salt,omitempty"`
	Role      string        `json:"role,omitempty" bson:"role,omitempty"`
	IsActive  bool          `json:"isActive,omitempty" bson:"isActive,omitempty"`
	CreatedAT int64         `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	UpdatedAT int64         `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
}

type Credential struct {
	Email string `json:"email"`
	Password string `json:"password"`
}

// compare incoming password and existing password
func(u *User) ComparePassword(password string) error {
	incoming := []byte(password  + u.Salt)
	existing := []byte(u.Password + u.Salt)
	err := bcrypt.CompareHashAndPassword(existing, incoming)
	return err
}

// validate user field
func (u *User) Validate() error {
	//validate name field with isRequired, minlength:3, maxlength:25 and no regex
	if e:= utility.ValidateRequireAndLengthAndRegex(u.Name, true, 3, 25, "", "Name"); e != nil {
		return e
	}

	// validate email field with isRequired, minlength:5, maxlength:25 and regex check
	if e := utility.ValidateRequireAndLengthAndRegex(u.Email, true, 5, 35, `^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`, "Email"); e != nil {
		return e
	}

	//validate password field with  isRequired, minlength:8, maxlength:25 and regex check
	if e := utility.ValidateRequireAndLengthAndRegex(u.Password, true, 8, 25, "^[a-zA-Z0-9_!@#$_%^&*.?()-=+]*$", "Password"); e != nil {
		return e
	}

	return nil

}

// Initialize will set password, createdAt and updatedAt
func(u *User) Initialize() error {
	salt := uuid.New().String()
	passwordBytes := []byte (u.Password + salt)
	hash, err := bcrypt.GenerateFromPassword(passwordBytes, bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	u.Password = string(hash[:])
	u.Salt = salt
	u.CreatedAT = utility.CurrentTimeInMilli()
	u.UpdatedAT = utility.CurrentTimeInMilli()
	u.IsActive = true
	u.Role = utility.UserRole
	return nil
}
