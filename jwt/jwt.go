package jwt

import (
	jwt "github.com/dgrijalva/jwt-go"
	"log"
	"mongodb-restful/config"
	"mongodb-restful/utility"
	"net/http"
	"strings"
)

type JwtToken struct {
	C *config.Configuration
}

type Token struct {
	ID string `json:"id"`
	Role string `json:"role"`
	jwt.StandardClaims
}

func(jt *JwtToken) CreateToken(id, role string) (map[string]string, error) {
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), &Token{
		ID: id,
		Role: role,
	})
	tokenString, err := token.SignedString([]byte(jt.C.JwtSecret))
	if err != nil {
		return nil, err
	}
	m := make(map[string]string)
	m["token"] = tokenString
	return m,nil
}

// ProtectedEndPoint  : authenticate all requests
func(jt *JwtToken) ProtectedEndPoint(h http.Handler) http.Handler {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {
		log.Println("middleware", r.URL)
		if strings.Contains(r.URL.Path, "/auth/") || strings.Contains(r.URL.Path, "/swagger") {
			h.ServeHTTP(w, r)
		} else {
			utility.Response(w, utility.NewHTTPError(utility.Unauthorized, http.StatusUnauthorized))
		}
	})
}
