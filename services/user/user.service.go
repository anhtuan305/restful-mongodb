package user

import (
	"gopkg.in/mgo.v2"
	"mongodb-restful/config"
	repository "mongodb-restful/repositories/user"
)

type UserServiceInterface interface {

}

//
type UserService struct {
	db         *mgo.Session
	repository repository.UserRepository
	config     *config.Configuration
}

// New function will initialize UserService
func New(userRepo repository.UserRepository) UserServiceInterface {
	return &UserService{repository: userRepo}
}