package auth
import (
	"context"
	_"context"
	"gopkg.in/mgo.v2/bson"
	"mongodb-restful/models"
	repository "mongodb-restful/repositories/user"


)

type AuthServiceInterface interface {
	Create(context.Context, *models.User) error
	IsUserAlreadyExist(context.Context,  string) bool
	Login(ctx context.Context, credential *models.Credential) (*models.User, error)
}

type AuthService struct {
	repository repository.UserRepository
}

func New(userRepo repository.UserRepository) AuthServiceInterface {
	return &AuthService{repository: userRepo}
}

// IsUserAlreadyExist check if user exist in DB
func (service *AuthService) IsUserAlreadyExist(ctx context.Context, email string) bool{
	return service.repository.IsUserAlreadyExists(ctx, email)
}

func (service *AuthService) Create (ctx context.Context, user *models.User) error{
	return service.repository.Create(ctx, user)
}

func (service *AuthService) Login(ctx context.Context, credential *models.Credential) (*models.User, error) {
	query := bson.M{"email": credential.Email}
	user, err := service.repository.FindOne(ctx, query)
	if err != nil {
		return nil, err
	}
	if err = user.ComparePassword(credential.Password); err != nil {
		return nil,err
	}
	return user, nil
}
