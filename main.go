package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"log"
	"mongodb-restful/config"
	db "mongodb-restful/database"
	"mongodb-restful/jwt"
	"mongodb-restful/middleware"
	"mongodb-restful/routes"
	"mongodb-restful/utility"
	"net/http"
	"time"
	httpSwagger "github.com/swaggo/http-swagger"
)
func main() {
	// initialize config
	conf := config.NewConfig()
	fmt.Printf("%+v\n", conf)

	//make connection with db and get instance
	dbSession := db.GetInstance(conf)

	//
	dbSession.SetSafe(&mgo.Safe{})

	// Router
	router := mux.NewRouter()
	routes.InitializeRoutes(router, dbSession, conf)

	// JWT service
	jwtService := jwt.JwtToken{conf}

	// add all request to authenticate into middleware
	router.Use(middleware.Cors, jwtService.ProtectedEndPoint)

	router.PathPrefix("/swagger").Handler(httpSwagger.WrapHandler)

	// Server configuration
	server := &http.Server{
		Handler: utility.Headers(router),
		Addr: conf.Address,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Application is running at", conf.Address)

	// Serving application at specified port
	log.Fatal(server.ListenAndServe())












}