package user

import (
	"context"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"mongodb-restful/config"
	model "mongodb-restful/models"
)

type UserRepositoryImp struct {
	db     *mgo.Session
	config *config.Configuration
}

// UseRepository used to perform db operations
// Interface contains basic operations on user document
type UserRepository interface{
	//Create will perform db operation to save user
	Create(context.Context, *model.User) error

	//
	FindOne(context.Context, interface{}) (*model.User, error)


	IsUserAlreadyExists(context.Context, string) bool

}
func New(db *mgo.Session, c *config.Configuration) UserRepository{
	return &UserRepositoryImp{db: db, config: c}
}

func (service *UserRepositoryImp) Create(ctx context.Context, user *model.User) error {
	return service.collection().Insert(user)

}

func (service *UserRepositoryImp) FindOne(ctx context.Context, query interface{}) (*model.User, error) {
	var user model.User
	e := service.collection().Find(query).One(&user)
	return &user, e

}

// IsUserAlreadyExists check if user exists in DB
func (service *UserRepositoryImp) IsUserAlreadyExists(ctx context.Context, email string) bool {
	query := bson.M{"email": email}
	_, e := service.FindOne(ctx, query)
	if e != nil {
		return false
	}
	return true
}

func (service *UserRepositoryImp) collection() *mgo.Collection {
	return service.db.DB(service.config.DataBaseName).C("users")
}

